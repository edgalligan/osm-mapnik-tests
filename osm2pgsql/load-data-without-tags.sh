#!/usr/bin/env bash
# vim: set et sw=4 ts=4 ff=unix ft=sh :

if [ -z "$1" ]
then
    echo "Please specify an OSM data file to load (.gz, .bz2, .pbf  .o5m)"
    echo "Exiting..."
    echo ""
    exit 1
fi

osm2pgsql "$1" -d osm -S empty.style