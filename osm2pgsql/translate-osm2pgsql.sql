DROP TABLE planet_osm_roads; -- Should be empty
ALTER TABLE planet_osm_point RENAME TO node_geometry;
ALTER TABLE planet_osm_polygon RENAME TO way_geometry;
INSERT INTO way_geometry SELECT * FROM planet_osm_line;
DROP TABLE planet_osm_line;

-- INSERT INTO way_geometry SELECT osm_id, z_order, 0.0, way FROM node_geometry;
-- DROP TABLE node_geometry;

INSERT INTO way SELECT osm_id, z_order, way_area FROM planet_osm_polygon WHERE osm_id > 0;
INSERT INTO way_metadata (osm_id, osm_uid, osm_version, osm_changeset) SELECT osm_id, osm_uid, osm_version, osm_changeset FROM planet_osm_polygon WHERE osm_id > 0;
INSERT INTO way_geometry (osm_id, way) SELECT osm_id, way FROM planet_osm_polygon WHERE osm_id > 0;


CREATE TABLE IF NOT EXISTS way (
    osm_id bigint NOT NULL,
    z_order int DEFAULT NULL,
    way_area real DEFAULT 0.0,
    osm_timestamp timestamp(0) with time zone,
    PRIMARY KEY (osm_id)
);

CREATE TABLE IF NOT EXISTS way_metadata (
    id bigint GENERATED ALWAYS AS IDENTITY (
        START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    ),
    osm_id bigint NOT NULL,
    osm_uid integer,
    osm_version integer,
    osm_changeset integer,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS way_geometry (
    id bigint GENERATED ALWAYS AS IDENTITY (
        START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    ),
    osm_id bigint NOT NULL,
    way geometry(Geometry,3857) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tag (
    id bigint GENERATED ALWAYS AS IDENTITY (
        START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    ),
    osm_id bigint NOT NULL,
    tag_key character varying(254) NOT NULL,
    tag_value character varying(254) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tag_text (
    id bigint GENERATED ALWAYS AS IDENTITY (
        START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    ),
    osm_id bigint NOT NULL,
    tag_key character varying(254) NOT NULL,
    tag_value text NOT NULL,
    PRIMARY KEY (id)
);


INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'access' as tag_key, `access` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `access` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'addr:housename' as tag_key, `addr:housename` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `addr:housename` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'addr:housenumber' as tag_key, `addr:housenumber` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `addr:housenumber` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'addr:interpolation' as tag_key, `addr:interpolation` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `addr:interpolation` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'admin_level' as tag_key, `admin_level` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `admin_level` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'aerialway' as tag_key, `aerialway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `aerialway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'aeroway' as tag_key, `aeroway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `aeroway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'amenity' as tag_key, `amenity` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `amenity` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'area' as tag_key, `area` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `area` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'barrier' as tag_key, `barrier` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `barrier` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'bicycle' as tag_key, `bicycle` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `bicycle` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'brand' as tag_key, `brand` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `brand` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'bridge' as tag_key, `bridge` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `bridge` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'boundary' as tag_key, `boundary` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `boundary` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'building' as tag_key, `building` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `building` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'construction' as tag_key, `construction` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `construction` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'covered' as tag_key, `covered` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `covered` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'culvert' as tag_key, `culvert` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `culvert` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'cutting' as tag_key, `cutting` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `cutting` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'denomination' as tag_key, `denomination` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `denomination` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'disused' as tag_key, `disused` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `disused` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'embankment' as tag_key, `embankment` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `embankment` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'foot' as tag_key, `foot` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `foot` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'generator:source' as tag_key, `generator:source` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `generator:source` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'harbour' as tag_key, `harbour` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `harbour` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'highway' as tag_key, `highway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `highway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'historic' as tag_key, `historic` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `historic` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'horse' as tag_key, `horse` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `horse` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'intermittent' as tag_key, `intermittent` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `intermittent` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'junction' as tag_key, `junction` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `junction` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'landuse' as tag_key, `landuse` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `landuse` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'layer' as tag_key, `layer` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `layer` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'leisure' as tag_key, `leisure` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `leisure` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'lock' as tag_key, `lock` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `lock` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'man_made' as tag_key, `man_made` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `man_made` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'military' as tag_key, `military` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `military` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'motorcar' as tag_key, `motorcar` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `motorcar` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'name' as tag_key, `name` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `name` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'natural' as tag_key, `natural` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `natural` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'office' as tag_key, `office` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `office` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'oneway' as tag_key, `oneway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `oneway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'operator' as tag_key, `operator` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `operator` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'place' as tag_key, `place` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `place` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'population' as tag_key, `population` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `population` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'power' as tag_key, `power` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `power` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'power_source' as tag_key, `power_source` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `power_source` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'public_transport' as tag_key, `public_transport` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `public_transport` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'railway' as tag_key, `railway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `railway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'ref' as tag_key, `ref` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `ref` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'religion' as tag_key, `religion` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `religion` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'route' as tag_key, `route` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `route` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'service' as tag_key, `service` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `service` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'shop' as tag_key, `shop` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `shop` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'sport' as tag_key, `sport` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `sport` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'surface' as tag_key, `surface` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `surface` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'toll' as tag_key, `toll` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `toll` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tourism' as tag_key, `tourism` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tourism` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tower:type' as tag_key, `tower:type` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tower:type` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tracktype' as tag_key, `tracktype` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tracktype` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tunnel' as tag_key, `tunnel` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tunnel` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'water' as tag_key, `water` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `water` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'sport' as tag_key, `sport` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `sport` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'surface' as tag_key, `surface` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `surface` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'toll' as tag_key, `toll` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `toll` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tourism' as tag_key, `tourism` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tourism` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tower:type' as tag_key, `tower:type` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tower:type` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tracktype' as tag_key, `tracktype` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tracktype` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'tunnel' as tag_key, `tunnel` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `tunnel` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'water' as tag_key, `water` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `water` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'waterway' as tag_key, `waterway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `waterway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'wetland' as tag_key, `wetland` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `wetland` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'width' as tag_key, `width` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `width` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'wood' as tag_key, `wood` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `wood` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'abandoned:aeroway' as tag_key, `abandoned:aeroway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `abandoned:aeroway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'abandoned:amenity' as tag_key, `abandoned:amenity` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `abandoned:amenity` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'abandoned:building' as tag_key, `abandoned:building` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `abandoned:building` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'abandoned:landuse' as tag_key, `abandoned:landuse` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `abandoned:landuse` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'abandoned:power' as tag_key, `abandoned:power` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `abandoned:power` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'area:highway' as tag_key, `area:highway` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `area:highway` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'note' as tag_key, `note` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `note` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'source' as tag_key, `source` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `source` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'source_ref' as tag_key, `source_ref` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `source_ref` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'attribution' as tag_key, `attribution` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `attribution` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'comment' as tag_key, `comment` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `comment` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'fixme' as tag_key, `fixme` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `fixme` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'created_by' as tag_key, `created_by` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `created_by` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'odbl' as tag_key, `odbl` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `odbl` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'odbl:note' as tag_key, `odbl:note` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `odbl:note` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'SK53_bulk:load' as tag_key, `SK53_bulk:load` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `SK53_bulk:load` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'accuracy:meters' as tag_key, `accuracy:meters` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `accuracy:meters` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'sub_sea:type' as tag_key, `sub_sea:type` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `sub_sea:type` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'waterway:type' as tag_key, `waterway:type` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `waterway:type` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, '3dshapes:ggmodelk' as tag_key, `3dshapes:ggmodelk` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `3dshapes:ggmodelk` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'AND_nosr_r' as tag_key, `AND_nosr_r` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `AND_nosr_r` IS NOT NULL;
INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, 'import' as tag_key, `import` as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND `import` IS NOT NULL;
