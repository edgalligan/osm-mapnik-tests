// vim: set et sw=4 ts=4 ff=unix ft=javascript :
// @ts-check

const { Client } = require('pg').native;

const dsn = 'postgresql://ed:password@localhost:5432/osm';
const db = new Client({connectionString: dsn});

(async () => {

    await db.connect();

    const ids = (await db.query('select osm_id from tag_text group by osm_id having count(*) > 2')).rows.map(row => row.osm_id);
    const ways = await db.query(`
        SELECT
            way.*,
            ST_AsGeoJSON(geo.way) as geometry
        FROM
            way
        LEFT JOIN
            way_geometry as geo
        ON
            way.osm_id = geo.osm_id
        WHERE
            way.osm_id IN (${ids.join(',')})
    `);
    const results = await Promise.all(ways.rows.map(way => {
        return new Promise(async (res, rej) => {
            const osm_id = way.osm_id;
            const result = Object.assign({}, way);
            result.geometry = JSON.parse(result.geometry);
            try {
                const tags = await db.query('SELECT * FROM tag_text WHERE osm_id = $1', [osm_id]);
                result.tags = {};
                tags.rows.forEach(row => {
                    result.tags[row.tag_key] = row.tag_value;
                });
                res(result);
            } catch (e) {
                rej(e);
            }
        });
    }));

    console.log(results);

    await db.end();

})();

