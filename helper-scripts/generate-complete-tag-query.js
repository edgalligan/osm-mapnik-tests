// vim: set et sw=4 ts=4 ff=unix ft=javascript :
// @ts-check

process.stdout.write([
    'access',
    'addr:housename',
    'addr:housenumber',
    'addr:interpolation',
    'admin_level',
    'aerialway',
    'aeroway',
    'amenity',
    'area',
    'barrier',
    'bicycle',
    'brand',
    'bridge',
    'boundary',
    'building',
    'construction',
    'covered',
    'culvert',
    'cutting',
    'denomination',
    'disused',
    'embankment',
    'foot',
    'generator:source',
    'harbour',
    'highway',
    'historic',
    'horse',
    'intermittent',
    'junction',
    'landuse',
    'layer',
    'leisure',
    'lock',
    'man_made',
    'military',
    'motorcar',
    'name',
    'natural',
    'office',
    'oneway',
    'operator',
    'place',
    'population',
    'power',
    'power_source',
    'public_transport',
    'railway',
    'ref',
    'religion',
    'route',
    'service',
    'shop',
    'sport',
    'surface',
    'toll',
    'tourism',
    'tower:type',
    'tracktype',
    'tunnel',
    'water',
    'sport',
    'surface',
    'toll',
    'tourism',
    'tower:type',
    'tracktype',
    'tunnel',
    'water',
    'waterway',
    'wetland',
    'width',
    'wood',
    'abandoned:aeroway',
    'abandoned:amenity',
    'abandoned:building',
    'abandoned:landuse',
    'abandoned:power',
    'area:highway',
    'note',
    'source',
    'source_ref',
    'attribution',
    'comment',
    'fixme',
    'created_by',
    'odbl',
    'odbl:note',
    'SK53_bulk:load',
    'accuracy:meters',
    'sub_sea:type',
    'waterway:type',
    '3dshapes:ggmodelk',
    'AND_nosr_r',
    'import',
    ].map(t => `INSERT INTO tag_text (osm_id, tag_key, tag_value) SELECT osm_id, '${t}' as tag_key, "${t}" as tag_value FROM planet_osm_polygon WHERE osm_id > 0 AND "${t}" IS NOT NULL;`)
.join("\n") + "\n");
